// import modernizer so it is included in main.js
import Modernizr from 'modernizr';

jQuery( document ).ready(function($) {
  $('div.column_icon_box.no-link').click(function(e) {
    e.preventDefault();
  })

  // sticky menu
  if ($(window).width() < 960) {
    $('.floating_menu_container').removeClass('sticky_btm');
  } else {
    $('.floating_menu_container').addClass('sticky_btm');
    $(window).scroll(function() {

      var footerTop = $('#Footer').offset().top;
      var bottomPos = $(window).scrollTop() + $(window).height()
      if (bottomPos >= footerTop) {
        $('.floating_menu_container').removeClass('sticky_btm');
      } else {
        $('.floating_menu_container').addClass('sticky_btm');
      }
    });
  }

  /*
   * Province dropdown and go to the page on RME page:
   * Add class to buttons or a tags:
   *   For dropoff location:
   *     .show_dropoff_select .dropoff_location
   *   For what is accepted:
   *     .show_dropoff_select .dropoff_items
   *   For contact page:
   *     .show_dropoff_select .dropoff_contacts
   *   For schedule a pick up (business page):
   *     .show_dropoff_select .dropoff_business
   */
  var btn_label;
  $('.show_dropoff_select').click(function(e) {
    e.preventDefault();

    // Close the mobile menu
    $('#Side_slide .close').click();

    // assign button text
    if ($(this).hasClass('dropoff_location')) {
      $('.dropoff_select_btn').data('url', '/where-can-i-recycle');
      btn_label = 'Drop-off Locations';
    } else if ($(this).hasClass('dropoff_items')) {
      $('.dropoff_select_btn').data('url', '/what-can-i-recycle');
      btn_label = 'What Can Be Recycled';
    } else if ($(this).hasClass('dropoff_contacts')) {
      $('.dropoff_select_btn').data('url', '/contact-us');
      btn_label = 'Contact';
    } else if ($(this).hasClass('dropoff_business')) {
      $('.dropoff_select_btn').data('url', '/businesses/electronics-recycling-at-work/');
      btn_label = 'a pick-up';
    }

    $('.dropoff_select_btn span').html(btn_label);
    $('.dropoff_select_container').fadeIn();
  });

  $('.dropoff_select_btn').click(function (e) {
    var province = $('.dropoff_select').val();
    var page = $(this).data("url");
    if (province == '') {
      $('.dropoff_select_wrap p.warning').show();
    } else {
      $('.dropoff_select_wrap p.warning').hide();
      var current_language = document.documentElement.lang;
      var url;
      if ( current_language == 'fr-CA' && ( province == "qc" || province == "nb" )) {
        url = 'https://www.recyclermeselectroniques.ca/' + province + page;
      } else {
        url = 'https://www.recyclemyelectronics.ca/' + province + page;
      }

      window.open(url);
    }
  })

  $('.dropoff_select_close').click(function(e) {
    e.preventDefault();
    $('.dropoff_select_container').fadeOut();
  })
  // dropoff_select END
});


// home page icon box height
jQuery( window ).load(function(){
  var box_height = 0;
   jQuery('.home .icon_box > a').each(function () {
     var this_height = jQuery(this).height();
     if (this_height > box_height) {
       box_height = this_height;
     }
   })
   jQuery('.home .icon_box > a').height(box_height);
});
