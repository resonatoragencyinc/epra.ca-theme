( function( wp ) {
  var Fragment = wp.element.Fragment;
  var registerPlugin = wp.plugins.registerPlugin;
  var PluginSidebar = wp.editPost.PluginSidebar;
  var PluginSidebarMoreMenuItem = wp.editPost.PluginSidebarMoreMenuItem;
  var el = wp.element.createElement;
  var Panel = wp.components.Panel;
  var PanelBody = wp.components.PanelBody;
  var PanelRow = wp.components.PanelRow;
  var Text = wp.components.TextControl;
  var Select = wp.components.SelectControl;
  var Color = wp.components.ColorPicker;
  var Radio = wp.components.RadioControl;
  var withSelect = wp.data.withSelect;
  var withDispatch = wp.data.withDispatch;
  var compose = wp.compose.compose;
  var localized = ResonatorPageMeta;
  var navMenus = [];

  localized.nav_menus.forEach( element => {
    navMenus.push( { label: element.name, value: element.term_id } )
  } );

  var SchemaField = compose(
    withDispatch( function( dispatch, props ) {
      return {
        setMetaFieldValue: function( value ) {
          dispatch( 'core/editor' ).editPost(
            { meta: { [ props.fieldName ]: value } }
          );
        }
      }
    } ),
    withSelect( function( select, props ) {
      return {
        metaFieldValue: select( 'core/editor' )
        .getEditedPostAttribute( 'meta' )
        [ props.fieldName ],
        }
    } )
  )
  ( function( props ) {
    return el( Text, {
      label: 'Type',
      value: props.metaFieldValue,
      onChange: function( content ) {
        props.setMetaFieldValue( content );
      },
    } );
  } );

  var MenuEnabledField = compose(
    withDispatch( function( dispatch, props ) {
      return {
        setMetaFieldValue: function( value ) {
          dispatch( 'core/editor' ).editPost(
            { meta: { [ props.fieldName ]: value } }
            );
          }
        }
    } ),
    withSelect( function( select, props ) {
      return {
        metaFieldValue: select( 'core/editor' )
        .getEditedPostAttribute( 'meta' )
        [ props.fieldName ],
      }
    } )
  )
  ( function( props ) {
    return el( Radio, {
      selected: props.metaFieldValue,
      options: [
        { label: 'Off', value: '0' },
        { label: 'Single', value: '1' },
        { label: 'Extend to children', value: '2' }
      ],
      onChange: function( content ) {
        props.setMetaFieldValue( content );
      },
    } );
  } );

  var MenuField = compose(
    withDispatch( function( dispatch, props ) {
      return {
        setMetaFieldValue: function( value ) {
          dispatch( 'core/editor' ).editPost(
            { meta: { [ props.fieldName ]: value } }
            );
          }
        }
    } ),
    withSelect( function( select, props ) {
      return {
        metaFieldValue: select( 'core/editor' )
        .getEditedPostAttribute( 'meta' )
        [ props.fieldName ],
      }
    } )
  )
  ( function( props ) {
    return el( Select, {
      label: 'Menu',
      value: props.metaFieldValue,
      options: navMenus,
      onChange: function( content ) {
        props.setMetaFieldValue( content );
      },
    } );
  } );

  var ColourEnabledField = compose(
    withDispatch( function( dispatch, props ) {
      return {
        setMetaFieldValue: function( value ) {
          dispatch( 'core/editor' ).editPost(
            { meta: { [ props.fieldName ]: value } }
            );
          }
        }
    } ),
    withSelect( function( select, props ) {
      return {
        metaFieldValue: select( 'core/editor' )
        .getEditedPostAttribute( 'meta' )
        [ props.fieldName ],
      }
    } )
  )
  ( function( props ) {
    return el( Radio, {
      selected: props.metaFieldValue,
      options: [
        { label: 'Off', value: '0' },
        { label: 'Single', value: '1' },
        { label: 'Extend to children', value: '2' }
      ],
      onChange: function( content ) {
        props.setMetaFieldValue( content );
      },
    } );
  } );

  var ColourField = compose(
    withDispatch( function( dispatch, props ) {
      return {
        setMetaFieldValue: function( value ) {
          dispatch( 'core/editor' ).editPost(
            { meta: { [ props.fieldName ]: value } }
            );
          }
        }
    } ),
    withSelect( function( select, props ) {
      return {
        metaFieldValue: select( 'core/editor' )
        .getEditedPostAttribute( 'meta' )
        [ props.fieldName ],
      }
    } )
  )
  ( function( props ) {
    return el( Color, {
      color: props.metaFieldValue,
      onChangeComplete: function( value ) {
        props.setMetaFieldValue( value.hex );
      },
    } );
  } );

  function Component() {
    return el(
      Fragment,
      {},
      el(
        PluginSidebarMoreMenuItem,
        {
            target: 'section-settings-sidebar',
        },
        'Section Settings'
      ),
      el( PluginSidebar, {
          name: 'section-settings-sidebar',
          title: 'Section Settings',
        },
        el( Panel, {
          className: 'section-settings-sidebar-content'
          },
          el( PanelBody, {
            title: "Schema.org",
            initialOpen: false
            },
            el( PanelRow, {},
              el( SchemaField,
                { fieldName: 'section_settings_schema' }
              )
            )
          ),
          el( PanelBody, {
            title: "Section Menu",
            icon: "menu",
            initialOpen: false
            },
            el( PanelRow, {},
              el( MenuEnabledField,
                { fieldName: 'section_settings_menu_enabled' }
              )
            ),
            el( PanelRow, {},
              el( MenuField,
                { fieldName: 'section_settings_menu' }
              )
            )
          ),
          el( PanelBody, {
            title: "Section Colour",
            icon: "art",
            initialOpen: false
            },
            el( PanelRow, {},
              el( ColourEnabledField,
                { fieldName: 'section_settings_colour_enabled' }
              )
            ),
            el( PanelRow, {},
              el( ColourField,
                { fieldName: 'section_settings_colour' }
              )
            )
          )
        )
      )
    )
  }
  registerPlugin( 'section-settings', {
      icon: 'welcome-widgets-menus',
      render: Component,
  } );
} 
)( window.wp );
        