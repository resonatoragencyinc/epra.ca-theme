(function() {
  var exists = document.getElementById("headervideo_canvas_output");

  if ( exists ) {
    var outputCanvas = document.getElementById('headervideo_output'),
      output = outputCanvas.getContext('2d'),
      bufferCanvas = document.getElementById('headervideo_buffer'),
      buffer = bufferCanvas.getContext('2d'),
      headerVideo = document.getElementById('headervideo'),
      width = outputCanvas.width,
      height = outputCanvas.height,
      interval;

    function processFrame() {
      buffer.drawImage(headerVideo, 0, 0);

      // this can be done without alphaData, except in Firefox which doesn't like it when image is bigger than the canvas
      var image = buffer.getImageData(0, 0, width, height),
        imageData = image.data,
        alphaData = buffer.getImageData(0, height, width, height).data;

      for (var i = 3, len = imageData.length; i < len; i = i + 4) {
        imageData[i] = alphaData[i-1];
      }

      output.putImageData(image, 0, 0, 0, 0, width, height);
    }

    function randomColourVal() {
      return Math.floor( Math.random() * 256 );
    }

    headerVideo.addEventListener('play', function() {
      clearInterval(interval);
      interval = setInterval(processFrame, 40)
    }, false);

    headerVideo.addEventListener('ended', function() {
      headerVideo.pause();
    }, false);

    // Show loading animation.
    var playPromise = headerVideo.play();

    if (playPromise !== undefined) {
      playPromise.then(_ => {
      // Automatic playback started!
      })
      .catch(error => {
        console.log(error.message);
      });
    }
  }
})();
