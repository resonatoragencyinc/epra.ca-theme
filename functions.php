<?php
/**
 * EPRA Child Theme
 *
 * @package EPRA Child Theme
 * @author Resonator Agency Inc.
 * @link https://resonator.ca
 */

// white label
define( 'WHITE_LABEL', true );

/**
 * Enqueue Styles
 */
function mfnch_enqueue_styles() {
	// enqueue the parent stylesheet
	// however we do not need this if it is empty
	// wp_enqueue_style('parent-style', get_template_directory_uri() .'/style.css');

	// enqueue the parent RTL stylesheet
	if ( is_rtl() ) {
		wp_enqueue_style(
			'mfn-rtl',
			get_template_directory_uri() . '/rtl.css'
		);
	}

	// enqueue the child stylesheet
	wp_dequeue_style( 'style' );
	wp_enqueue_style(
		'style',
		get_stylesheet_directory_uri() . '/style.css'
	);
	wp_enqueue_style(
		'epra-style',
		get_stylesheet_directory_uri() . '/dist/css/style.min.css'
	);

	wp_enqueue_script(
		'epra-main-js',
		get_stylesheet_directory_uri() . '/dist/js/main.js',
		array(),
		false,
		true
	);

	wp_enqueue_script(
		'epra-footer-js',
		get_stylesheet_directory_uri() . '/dist/js/footer.js',
		array( 'epra-main-js' ),
		false,
		true
	);
}
add_action( 'wp_enqueue_scripts', 'mfnch_enqueue_styles', 101 );


/**
 * Enqueue Meta Block script
 */
function admin_enqueue() {
	wp_register_script(
		'resonator-page-meta',
		get_stylesheet_directory_uri() . '/dist/js/page-meta.js',
		array( 'wp-i18n', 'wp-plugins', 'wp-edit-post', 'wp-element', 'wp-components', 'wp-editor', 'wp-compose' )
	);
	wp_localize_script( 'resonator-page-meta', 'ResonatorPageMeta', array( 'nav_menus' => get_terms( 'nav_menu' ) ) );
	wp_enqueue_script( 'resonator-page-meta' );
}
add_action( 'enqueue_block_editor_assets', 'admin_enqueue' );


/**
 * Load Textdomain
 */
function mfnch_textdomain() {
	load_child_theme_textdomain(
		'betheme',
		get_stylesheet_directory() . '/languages'
	);
	load_child_theme_textdomain(
		'mfn-opts',
		get_stylesheet_directory() . '/languages'
	);
	load_child_theme_textdomain(
		'epra-theme',
		get_stylesheet_directory() . '/languages'
	);
}
add_action( 'after_setup_theme', 'mfnch_textdomain' );


/*
 * Include additional functions
 */
$includes = [
	'functions/theme-meta-blocks.php',
	'functions/theme-custom-shortcodes.php',
];

foreach ( $includes as $file ) {
	$filepath = locate_template( $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating %1$s for inclusion %2$s', esc_attr( $file ), esc_attr( $filepath ) ), E_USER_ERROR );
	}

	require_once $filepath;
}
unset( $file, $filepath );


/**
 * Disabled TGMPA and BeTheme register nags
 */
function remove_nag_messages() {
	// TGMPA nag screen
	global $Mfn_TGMPA;
	remove_action(
		'tgmpa_register',
		array( $Mfn_TGMPA, 'tgmpa_register' ),
		10
	);

	// Register BeTheme nag screen
	global $mfn_dashboard;
	remove_action(
		'admin_notices',
		array( $mfn_dashboard, 'admin_notices' ),
		1
	);
}
add_action( 'admin_head', 'remove_nag_messages' );
