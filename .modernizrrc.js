module.exports = {
  options: [
    "setClasses"
  ],
  "feature-detects": [
    "css/backgroundblendmode",
    "css/flexbox",
    "svg",
    "css/cssgrid",
    "canvas",
    "video/autoplay"
  ]
};