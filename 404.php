<?php
/**
 * 404 page.
 *
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */

$translate['404-title'] = mfn_opts_get( 'translate' ) ? mfn_opts_get( 'translate-404-title', 'Ooops... Error 404' ) : __( 'Ooops... Error 404', 'betheme' );
$translate['404-subtitle'] = mfn_opts_get( 'translate' ) ? mfn_opts_get( 'translate-404-subtitle', 'We are sorry, but the page you are looking for does not exist' ) : __( 'We are sorry, but the page you are looking for does not exist', 'betheme' );
$translate['404-text'] = mfn_opts_get( 'translate' ) ? mfn_opts_get( 'translate-404-text', 'Please check entered address and try again or' ) : __( 'Please check entered address and try again or ', 'betheme' );
$translate['404-btn'] = mfn_opts_get( 'translate' ) ? mfn_opts_get( 'translate-404-btn', 'go to homepage' ) : __( 'go to homepage', 'betheme' );
$custom_id = mfn_opts_get( 'error404-page' );

get_header();
?>

<div id="Content">
	<!-- Remove SVG tempurary -->
	<!-- <div class="content_wrapper clearfix" id="circuitboard">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="200 950 1116.4 2370.9" class="sprite sprite-circuitboard">
		  	<use xlink:href="<?php echo esc_html( get_stylesheet_directory_uri() ); ?>/dist/svg/sprites.svg#sprite-circuitboard"></use>
		</svg>
	</div> -->
	<div class="content_wrapper clearfix">

		<div class="sections_group">
			<div class="entry-content" itemprop="mainContentOfPage">
				<section class="section_wrapper">
					<h1 class="page_title">
						<?php the_title(); ?>
					</h1>
				</section>
				<div class="section">
					<div class="section_wrapper mcb-section-inner">
						<div class="wrap mcb-wrap one valign-top clearfix">
							<div class="column one">
								<div class="error_pic">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2247.32 757.67" class="sprite sprite-broken-wires">
									  	<use xlink:href="<?php echo esc_html( get_stylesheet_directory_uri() ); ?>/dist/svg/sprites.svg#sprite-broken-wires"></use>
									</svg>
								</div>
								<div class="error_desk">
									<h1>404 - Page Not Found</h1>
									<h4>We're not sure what you were trying to find, but we hope you find it! 😊</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php get_sidebar(); ?>

	</div>
</div>
<?php
get_footer();
