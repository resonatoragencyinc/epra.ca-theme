<?php
/**
 * The template for displaying the footer.
 *
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */

$back_to_top_class = mfn_opts_get( 'back-top-top' );

if ( 'hide' == $back_to_top_class ) {
	$back_to_top_position = false;
} elseif ( strpos( $back_to_top_class, 'sticky' ) !== false ) {
	$back_to_top_position = 'body';
} elseif ( mfn_opts_get( 'footer-hide' ) == 1 ) {
	$back_to_top_position = 'footer';
} else {
	$back_to_top_position = 'copyright';
}
?>

<?php do_action( 'mfn_hook_content_after' ); ?>

<?php if ( 'hide' != mfn_opts_get( 'footer-style' ) ) : ?>

	<footer id="Footer" class="clearfix">

		<?php
		$footer_call_to_action = mfn_opts_get( 'footer-call-to-action' );
		if ( $footer_call_to_action ) :
			?>
		<div class="footer_action">
			<div class="container">
				<div class="column one column_column">
					<?php echo do_shortcode( $footer_call_to_action ); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<?php
			$sidebars_count = 0;
		for ( $i = 1; $i <= 5; $i++ ) {
			if ( is_active_sidebar( 'footer-area-' . $i ) ) {
				$sidebars_count++;
			}
		}

		if ( $sidebars_count > 0 ) {

			echo '<div class="widgets_wrapper">';
				echo '<div class="container">';

			$footer_layout = '2;one-sixth;five-sixth';
			if ( $footer_layout ) {

				// Theme Options

				$footer_layout = explode( ';', $footer_layout );
				$footer_cols   = $footer_layout[0];

				echo '<div class="column one">';
					dynamic_sidebar( 'footer-area-2' );
				echo '</div>';

				echo '<div class="column one-sixth">';
				dynamic_sidebar( 'footer-area-1' );
				echo '</div>';

				echo '<div class="column five-sixth">';
				dynamic_sidebar( 'footer-area-3' );
				echo '</div>';
			} else {

					// default with equal width

					$sidebar_class = '';
				switch ( $sidebars_count ) {
					case 2:
								$sidebar_class = 'one-second';
						break;
					case 3:
								$sidebar_class = 'one-third';
						break;
					case 4:
								$sidebar_class = 'one-fourth';
						break;
					case 5:
								$sidebar_class = 'one-fifth';
						break;
					default:
									$sidebar_class = 'one';
				}

				for ( $i = 1; $i <= 5; $i++ ) {
					if ( is_active_sidebar( 'footer-area-' . $i ) ) {
						echo '<div class="column ' . esc_attr( $sidebar_class ) . '">';
							dynamic_sidebar( 'footer-area-' . $i );
						echo '</div>';
					}
				}
			}

				echo '</div>';
				echo '</div>';
		}
		?>

		<?php if ( mfn_opts_get( 'footer-hide' ) != 1 ) : ?>

			<div class="footer_copy">
				<div class="container">
					<div class="column one">

						<?php
						if ( 'copyright' == $back_to_top_position ) {
							echo '<a id="back_to_top" class="button button_js" href=""><i class="icon-up-open-big"></i></a>';
						}
						?>

						<div class="copyright">
							<?php
							if ( mfn_opts_get( 'footer-copy' ) ) {
								echo do_shortcode( mfn_opts_get( 'footer-copy' ) );
							} else {
								echo '&copy; ' . esc_html( date( 'Y' ) ) . ' ' . esc_html( get_bloginfo( 'name' ) ) . '. All Rights Reserved. <a target="_blank" rel="nofollow" href="https://muffingroup.com">Muffin group</a>';
							}
							?>
						</div>

						<?php
						if ( has_nav_menu( 'social-menu-bottom' ) ) {
							mfn_wp_social_menu_bottom();
						} else {
							get_template_part( 'includes/include', 'social' );
						}
						?>

					</div>
				</div>
			</div>

		<?php endif; ?>

		<?php
		if ( 'footer' == $back_to_top_position ) {
			echo '<a id="back_to_top" class="button button_js in_footer" href=""><i class="icon-up-open-big"></i></a>';
		}
		?>

	</footer>
<?php endif; ?>

<!-- drop off select box -->
<div class="dropoff_select_container">
	<div class="dropoff_select_box">
		<div class="dropoff_select_title">
			<?php _e( 'Select Your Province', 'epra-theme' ); ?>
			<a href="#" class="dropoff_select_close">X</a>
		</div>
		<div class="dropoff_select_wrap">
			<i class="icon-down-open"></i>
			<select class="dropoff_select" name="">
				<option value="" selected="selected"><?php _e( 'Province', 'epra-theme' ); ?></option>
				<option value="bc"><?php _e( 'British Columbia', 'epra-theme' ); ?></option>
				<option value="sk"><?php _e( 'Saskatchewan', 'epra-theme' ); ?></option>
				<option value="mb"><?php _e( 'Manitoba', 'epra-theme' ); ?></option>
				<option value="on"><?php _e( 'Ontario', 'epra-theme' ); ?></option>
				<option value="qc"><?php _e( 'Québec', 'epra-theme' ); ?></option>
				<option value="nb"><?php _e( 'New Brunswick', 'epra-theme' ); ?></option>
				<option value="ns"><?php _e( 'Nova Scotia', 'epra-theme' ); ?></option>
				<option value="pei"><?php _e( 'Prince Edward Island', 'epra-theme' ); ?></option>
				<option value="nl"><?php _e( 'Newfoundland &amp; Labrador', 'epra-theme' ); ?></option>
			</select>
			<p class="warning"><?php _e( 'Please select a province.', 'epra-theme' ); ?></p>
		</div>
		<button type="button" class="dropoff_select_btn"><?php _e( 'Find Drop-Off Locations', 'epra-theme' ); ?></button>
	</div>
</div>

</div>

<?php
	// side slide menu
if ( mfn_opts_get( 'responsive-mobile-menu' ) ) {
	get_template_part( 'includes/header', 'side-slide' );
}
?>

<?php
if ( 'body' == $back_to_top_position ) {
	echo '<a id="back_to_top" class="button button_js ' . esc_attr( $back_to_top_class ) . '" href=""><i class="icon-up-open-big"></i></a>';
}
?>

<?php if ( mfn_opts_get( 'popup-contact-form' ) ) : ?>
	<div id="popup_contact">
		<a class="button button_js" href="#"><i class="<?php echo esc_attr( mfn_opts_get( 'popup-contact-form-icon', 'icon-mail-line' ) ); ?>"></i></a>
		<div class="popup_contact_wrapper">
			<?php echo do_shortcode( mfn_opts_get( 'popup-contact-form' ) ); ?>
			<span class="arrow"></span>
		</div>
	</div>
<?php endif; ?>

<?php do_action( 'mfn_hook_bottom' ); ?>

<?php wp_footer(); ?>

</body>
</html>
