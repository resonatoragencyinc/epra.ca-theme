<?php
/**
 * Custom shortcodes
 *
 * @package epra
 * @author Resonator Agency Inc.
 * @link https://resonator.ca
 */

/**
 * Gets the current year
 *
 * @return string $year
 */
function year_shortcode() {
	$year = date( 'Y' );
	return $year;
}
add_shortcode( 'year', 'year_shortcode' );

/**
 * Video for the header
 * [header_video mp4="" width="" height=""]
 */
function header_video_shortcode( $atts ) {
	$atts = shortcode_atts(
		array(
			'mp4'  => '',
			'width'  => '',
			'height' => '',
		),
		$atts,
		'headervideo'
	);

	static $instance = 0;
	if ( $instance > 0 ) {
		return 'Only a single instance of this shortcode can be used at a time on a page.';
	}
	$instance++;

	if ( ! is_numeric( $atts['width'] ) || ! is_numeric( $atts['height'] ) ) {
		return '<p>Width and height must be set and only contain the digits 0-9.</p>';
	}

	$video = explode( '.', $atts['mp4'] );
	if ( end( $video ) !== 'mp4' ) {
		return '<p>Video file must be an mp4 encoded using the codec H.264.</p>';
	}

	$full_height = (int) $atts['height'] * 2;

	$style = "
	<style type='text/css'>
		#headervideo_canvas_output {
			z-index: 10;
			display: block;
			max-width: {$atts['width']}px;
			width: 100%;
			margin: 0 auto;
		}

		#headervideo_buffer {
			display: none;
		}

		#headervideo_output {
			max-width: {$atts['width']}px;
			max-height: {$atts['height']}px;
			width: 100%;
		}
	</style>\n\t\t";

	$output = apply_filters( 'headervideo_style', $style );

	$output         .= '<div id="headervideo_canvas_output">';
		$output     .= '<video id="headervideo" style="display:none" autoplay muted playsinline>';
			$output .= "<source src=\"{$atts['mp4']}\" type='video/mp4; codecs=\"avc1.42E01E\"' />";
		$output     .= '</video>';
		$output     .= "<canvas width=\"{$atts['width']}\" height=\"{$full_height}\" id=\"headervideo_buffer\"></canvas>";
		$output     .= "<canvas width=\"{$atts['width']}\" height=\"{$atts['height']}\" id=\"headervideo_output\"></canvas>";
	$output         .= '</div>';

	return $output;
}
add_shortcode( 'header_video', 'header_video_shortcode' );
