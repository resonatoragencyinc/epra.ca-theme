<?php
/**
 * Page Meta
 *
 * @package EPRA Child Theme
 * @author Resonator Agency Inc.
 * @link https://resonator.ca
 */

/**
 * Registers the sidebar page meta keys
 *
 * @return void
 */
function sidebar_plugin_register() {
	register_meta(
		'post',
		'section_settings_schema',
		array(
			'show_in_rest' => true,
			'single'       => true,
			'type'         => 'string',
		)
	);
	register_meta(
		'post',
		'section_settings_menu_enabled',
		array(
			'show_in_rest' => true,
			'single'       => true,
			'type'         => 'string',
		)
	);
	register_meta(
		'post',
		'section_settings_menu',
		array(
			'show_in_rest' => true,
			'single'       => true,
			'type'         => 'string',
		)
	);
	register_meta(
		'post',
		'section_settings_colour_enabled',
		array(
			'show_in_rest' => true,
			'single'       => true,
			'type'         => 'string',
		)
	);
	register_meta(
		'post',
		'section_settings_colour',
		array(
			'show_in_rest' => true,
			'single'       => true,
			'type'         => 'string',
		)
	);
}
add_action( 'init', 'sidebar_plugin_register' );
