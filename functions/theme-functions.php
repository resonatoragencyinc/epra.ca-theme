<?php
/**
 * Theme functions
 *
 * @package epra
 * @author Resonator Agency Inc.
 * @link https://resonator.ca
 */

// phpcs:disable
/**
 * Schema | Auto Get Schema Type By Post Type
 */

function mfn_tag_schema() {
	$schema = 'https://schema.org/';

	// Is Woocommerce product.
	if ( function_exists( 'is_product' ) && is_product() ) {
		$type = false;
	} elseif ( is_single() && get_post_type() == 'post' ) {
		// Single post.
		$type = 'Article';
	} elseif ( is_author() ) {
		// Author page.
		$type = 'ProfilePage';
	} elseif ( is_search() ) {
		// Search results.
		$type = 'SearchResultsPage';
	} else {
		// Resonator - Added custom schema tags.
		$type = get_post_meta( get_the_ID(), 'section_settings_schema', true ) ?? get_post_meta( get_the_ID(), 'section_settings_schema', true ) ?? 'WebPage';
	}

	if ( mfn_opts_get( 'mfn-seo-schema-type' ) && $type ) {
		echo ' itemscope itemtype="' . esc_url( $schema ) . esc_attr( $type ) . '"';
	}

	return true;
}
// phpcs:enable

//phpcs:disable
/**
 * Breadcrumbs
 * Added schema tags
 */

function mfn_breadcrumbs( $class = false ) {
	global $post;

	$breadcrumbs = array();
	$separator   = ' <span> /&nbsp; </span>';

	// translate.
	$translate['home'] = mfn_opts_get( 'translate' ) ? mfn_opts_get( 'translate-home', 'Home' ) : __( 'Home', 'betheme' );

	// plugin: bbPress.
	if ( function_exists( 'is_bbpress' ) && is_bbpress() ) {
		bbp_breadcrumb(
			array(
				'before'       => '<ul class="breadcrumbs">',
				'after'        => '</ul>',
				'sep'          => '<i class="icon-right-open"></i>',
				'crumb_before' => '<li>',
				'crumb_after'  => '</li>',
				'home_text'    => esc_html( $translate['home'] ),
			)
		);
		return true; // exit.
	}

	// home prefix.
	$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_attr( home_url() ) . '"><span itemprop="name">' . esc_html( $translate['home'] ) . '<span></a>';

	// blog.
	if ( 'post' === get_post_type() ) {
		$blog_id = false;

		if ( get_option( 'page_for_posts' ) ) {
			$blog_id = get_option( 'page_for_posts' );   // Setings / Reading.
		}

		if ( $blog_id ) {
			$blog_post = get_post( $blog_id );

			// blog Page has parent.
			if ( $blog_post && $blog_post->post_parent ) {

				$parent_id = $blog_post->post_parent;
				$parents   = array();

				while ( $parent_id ) {
					$page      = get_page( $parent_id );
					$parents[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . get_permalink( $page->ID ) . '"><span itemprop="name">' . wp_kses( get_the_title( $page->ID ), mfn_allowed_html() ) . '<span></a>';
					$parent_id = $page->post_parent;
				}

				$parents     = array_reverse( $parents );
				$breadcrumbs = array_merge_recursive( $breadcrumbs, $parents );
			}

			$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( get_permalink( $blog_id ) ) . '"><span itemprop="name">' . wp_kses( get_the_title( $blog_id ), mfn_allowed_html() ) . '<span></a>';
		}
	}

	if ( is_front_page() || is_home() ) {
		// do nothing.
	} elseif ( function_exists( 'tribe_is_month' ) && ( tribe_is_event_query() || tribe_is_month() || tribe_is_event() || tribe_is_day() || tribe_is_venue() ) ) {
		// plugin: Events Calendar.
		if ( function_exists( 'tribe_get_events_link' ) ) {
			$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( tribe_get_events_link() ) . '"><span itemprop="name">' . esc_html( tribe_get_events_title() ) . '<span></a>';
		}
	} elseif ( is_tag() ) {
		// blog: tag.
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . esc_html( single_tag_title( '', false ) ) . '<span></a>';
	} elseif ( is_category() ) {
		// blog: category.
		$cat = get_term_by( 'name', single_cat_title( '', false ), 'category' );
		if ( $cat && $cat->parent ) {
			$breadcrumbs[] = get_category_parents( $cat->parent, true, $separator );
		}
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . esc_html( single_cat_title( '', false ) ) . '<span></a>';
	} elseif ( is_author() ) {
		// blog: author.
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . esc_html( get_the_author() ) . '<span></a>';
	} elseif ( is_day() ) {
		// blog: day.
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( get_year_link( get_the_time( 'Y' ) ) ) . '"><span itemprop="name">' . esc_html( get_the_time( 'Y' ) ) . '<span></a>';
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) ) . '"><span itemprop="name">' . esc_html( get_the_time( 'F' ) ) . '<span></a>';
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . esc_html( get_the_time( 'd' ) ) . '<span></a>';
	} elseif ( is_month() ) {
		// blog: month.
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( get_year_link( get_the_time( 'Y' ) ) ) . '"><span itemprop="name">' . esc_html( get_the_time( 'Y' ) ) . '<span></a>';
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . esc_html( get_the_time( 'F' ) ) . '<span></a>';
	} elseif ( is_year() ) {
		// blog: year.
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . esc_html( get_the_time( 'Y' ) ) . '<span></a>';
	} elseif ( is_single() && ! is_attachment() ) {
		// single.
		if ( get_post_type() != 'post' ) {
			// portfolio.
			$post_type         = get_post_type_object( get_post_type() );
			$slug              = $post_type->rewrite;
			$portfolio_page_id = mfn_wpml_ID( mfn_opts_get( 'portfolio-page' ) );

			// portfolio page.
			if ( $slug['slug'] == mfn_opts_get( 'portfolio-slug', 'portfolio-item' ) && $portfolio_page_id ) {
				$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( get_page_link( $portfolio_page_id ) ) . '"><span itemprop="name">' . esc_html( get_the_title( $portfolio_page_id ) ) . '<span></a>';
			}

			// category.
			if ( $portfolio_page_id ) {
				$terms = get_the_terms( get_the_ID(), 'portfolio-types' );
				if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
					$term          = $terms[0];
					$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( get_term_link( $term ) ) . '"><span itemprop="name">' . esc_html( $term->name ) . '<span></a>';
				}
			}

			// single.
			$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . get_the_title() . '<span></a>';
		} else {
			// blog single.
			$cat = get_the_category();
			if ( ! empty( $cat ) ) {
				$breadcrumbs[] = get_category_parents( $cat[0], true, $separator );
			}

			$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . get_the_title() . '<span></a>';
		}
	} elseif ( ! is_page() && get_post_taxonomies() ) {
		// taxonomy portfolio.
		$post_type = get_post_type_object( get_post_type() );
		if ( $post_type->name == 'portfolio' && $portfolio_page_id = mfn_wpml_ID( mfn_opts_get( 'portfolio-page' ) ) ) {
			$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( get_page_link( $portfolio_page_id ) ) . '"><span itemprop="name">' . esc_html( get_the_title( $portfolio_page_id ) ) . '<span></a>';
		}

		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . esc_html( single_cat_title( '', false ) ) . '<span></a>';
	} elseif ( is_page() && $post->post_parent ) {
		// page with parent.
		$parent_id = $post->post_parent;
		$parents   = array();

		while ( $parent_id ) {
			$page      = get_page( $parent_id );
			$parents[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( get_permalink( $page->ID ) ) . '"><span itemprop="name">' . wp_kses( get_the_title( $page->ID ), mfn_allowed_html() ) . '<span></a>';
			$parent_id = $page->post_parent;
		}
		$parents     = array_reverse( $parents );
		$breadcrumbs = array_merge_recursive( $breadcrumbs, $parents );

		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . wp_kses( get_the_title( mfn_ID() ), mfn_allowed_html() ) . '<span></a>';
	} else {
		// default.
		$breadcrumbs[] = '<a itemtype="https://schema.org/Thing" itemprop="item" href="' . esc_url( mfn_current_URL() ) . '"><span itemprop="name">' . wp_kses( get_the_title( mfn_ID() ), mfn_allowed_html() ) . '<span></a>';
	}

	// output -----.
	echo '<ul class="breadcrumbs ' . esc_attr( $class ) . '" itemscope itemtype="https://schema.org/BreadcrumbList">';
		$count = count( $breadcrumbs );
		$i     = 1;

	foreach ( $breadcrumbs as $bk => $bc ) {
		if ( strpos( $bc, $separator ) ) {
			// category parent.
			echo '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">' . $bc;
			echo '<meta itemprop="position" content="' . $i . '" /></li>';
		} else {
			if ( $i == $count ) {
				$separator = '';
			}
			echo '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">' . $bc . $separator;
			echo '<meta itemprop="position" content="' . $i . '" /></li>';
		}

		$i++;
	}

	echo '</ul>';
}
// phpcs:enable

/**
 * GET ORGINAL THEME-HEAD.PHP
 */
require_once get_template_directory() . '/functions/theme-functions.php';

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
* Filter function used to remove the tinymce emoji plugin.
*
* @param array $plugins
* @return array Difference betwen the two arrays
*/
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
* Remove emoji CDN hostname from DNS prefetching hints.
*
* @param array $urls URLs to print for resource hints.
* @param string $relation_type The relation type the URLs are printed for.
* @return array Difference betwen the two arrays.
*/
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/* This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}

	return $urls;
}

/**
 * Add province to header menu
 */
// add_filter('wp_nav_menu_items','add_province_select_to_menu', 10, 2);
function add_province_select_to_menu( $items, $args ) {
	if ( 'main-menu' == $args->theme_location ) {
		return $items . '<li class="province_select_wrapper"><i class="icon-down-open"></i><select><option value="on">ON</option></select></li>';
	}
}

/**
 * Add stickey footer menu
 */
function resonator_floating_menu() {
	register_nav_menu( 'footer-floating-menu', __( 'Footer Floating Menu' ) );
}
add_action( 'init', 'resonator_floating_menu' );

function resonator_add_sticky_menu() {
	$locations = get_nav_menu_locations();

	if ( isset( $locations['footer-floating-menu'] ) ) :
		$menu = wp_get_nav_menu_object( $locations['footer-floating-menu'] );
		$menu_items = wp_get_nav_menu_items( $menu->term_id );
		?>
		<div class="container floating_menu_container"><ul class="floating_menu">
		<?php
		foreach ( $menu_items as $menu_item ) :
			?>
			<li><a href="<?php echo esc_html( $menu_item->url ); ?>" target="<?php echo esc_html( $menu_item->target ); ?>">
				<?php echo do_shortcode( '[icon type="icon-' . $menu_item->xfn . '"]' ); ?>
				<span><?php echo esc_html( $menu_item->title ); ?></span>
			</a></li>
		<?php endforeach; ?>
		</ul></div>
		<?php
	endif;
}
add_action( 'mfn_hook_content_after', 'resonator_add_sticky_menu' );
