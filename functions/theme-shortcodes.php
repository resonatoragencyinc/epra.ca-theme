<?php

/**
 * FAQ [faq][faq_item]../[/faq]
 * Added schema tags
 */
function sc_faq( $attr, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'title'   => '',
				'tabs'    => '',
				'open1st' => '',
				'openall' => '',
				'openAll' => '',
			),
			$attr
		)
	);

	// class

	$class = '';
	if ( $open1st ) {
		$class .= ' open1st';
	}
	if ( $openall || $openAll ) {
		$class .= ' openAll';
	}

	// output -----

	$output = '<div class="faq">';

	if ( $title ) {
		$output .= '<h4 class="title">' . wp_kses( $title, mfn_allowed_html() ) . '</h4>';
	}
		$output .= '<div class="mfn-acc faq_wrapper ' . esc_attr( $class ) . '">';

	if ( is_array( $tabs ) ) {
		// content builder
		$i = 0;

		foreach ( $tabs as $tab ) {
			$i++;

			$output .= '<div class="question" itemscope itemprop="mainEntity" itemtype="http://schema.org/Question">';

				$output     .= '<div class="title">';
					$output .= '<span class="num">' . esc_html( $i ) . '</span>';
					$output .= '<i class="icon-plus acc-icon-plus"></i>';
					$output .= '<i class="icon-minus acc-icon-minus"></i>';
					$output .= '<span itemprop="name">' . wp_kses( $tab['title'], mfn_allowed_html() ) . '</span>';
				$output     .= '</div>';

				$output     .= '<div class="answer" itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Answer">';
					$output .= '<span itemprop="text">' . do_shortcode( $tab['content'] ) . '</span>';
				$output     .= '</div>';

			$output .= '</div>' . "\n";
		}
	} else {
		// shortcode
		$output .= do_shortcode( $content );
	}

		$output .= '</div>';

	$output .= '</div>' . "\n";

	return $output;
}

/**
 * FAQ Item [faq_item][/faq_item]
 * Added schema tags
 */

function sc_faq_item( $attr, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'title'  => '',
				'number' => '1',
			),
			$attr
		)
	);

	// output

	$output = '<div class="question" itemscope itemtype="http://schema.org/Question">';

		$output     .= '<div class="title">';
			$output .= '<span class="num">' . esc_html( $number ) . '</span>';
			$output .= '<i class="icon-plus acc-icon-plus"></i>';
			$output .= '<i class="icon-minus acc-icon-minus"></i>';
			$output .= wp_kses( $title, mfn_allowed_html() );
		$output     .= '</div>';

		$output     .= '<div class="answer" itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Answer">';
			$output .= do_shortcode( $content );
		$output     .= '</div>';

	$output .= '</div>' . "\n";

	return $output;
}


/**
 * Flat Box [feature_box] [/feature_box]
 */

function sc_feature_box( $attr, $content = null ) {
	extract(
		shortcode_atts(
			array(
				'image'          => '',
				'title'          => '',
				'background'     => '',
				'link'           => '',
				'target'         => '',
				'animate'        => '',
				'image_position' => '',
			),
			$attr
		)
	);

	// image | visual composer fix

	$image = mfn_vc_image( $image );

	// target

	if ( $target == 'lightbox' ) {
		$target_escaped = 'rel="prettyphoto"';
	} elseif ( $target ) {
		$target_escaped = 'target="_blank"';
	} else {
		$target_escaped = false;
	}

	// background

	if ( $background ) {
		$background_escaped = 'style="background-color:' . esc_attr( $background ) . '"';
	} else {
		$background_escaped = false;
	}

	// output -----

	$output = '<div class="feature_box">';

	$output .= '<hr>';

	if ( $animate ) {
		$output .= '<div class="animate" data-anim-type="' . esc_attr( $animate ) . '">';
	}

			// This variable has been safely escaped above in this function
			$output .= '<div class="feature_box_wrapper" ' . $background_escaped . '>';

				$output .= '<div class="photo_wrapper">';

	if ( $link ) {
		// This variable has been safely escaped above in this function
		$output .= '<a href="' . esc_url( $link ) . '" ' . $target_escaped . '>';
	}

					$output .= '<img class="scale-with-grid" src="' . esc_url( $image ) . '" alt="' . esc_attr( mfn_get_attachment_data( $image, 'alt' ) ) . '" width="' . esc_attr( mfn_get_attachment_data( $image, 'width' ) ) . '" height="' . esc_attr( mfn_get_attachment_data( $image, 'height' ) ) . '" />';

	if ( $link ) {
		$output .= '</a>';
	}

				$output .= '</div>';

				$output .= '<div class="desc_wrapper">';

	if ( $title ) {
		$output .= '<h4>' . wp_kses( $title, mfn_allowed_html() ) . '</h4>';
	}

	if ( $content ) {
		$output .= '<div class="desc">' . do_shortcode( $content ) . '</div>';
	}

				$output .= '</div>';

			$output .= '</div>';

	if ( $animate ) {
		$output .= '</div>';
	}

	$output .= '</div>' . "\n";

	return $output;
}

/**
 * GET ORGINAL THEME-HEAD.PHP
 */
require_once get_template_directory() . '/functions/theme-shortcodes.php';
