<?php
/**
 * Theme head
 *
 * @package epra
 * @author Resonator Agency Inc.
 * @link https://resonator.ca
 */

/**
 * Adds CSS to add an icon fter any external links
 *
 * @return void
 */
function resonator_external_link_marker() {
	$protocol   = ( is_ssl() ) ? 'https' : 'http';
	$custom_css = '
		#Header_wrapper a:not([href*=\'' . get_site_url( get_current_blog_id(), '', $protocol ) . '\']):not([href^=\'#\']):not([href^=\'/\']):not(.no-external-icon)::after,
		#Footer a:not([href*=\'' . get_site_url( get_current_blog_id(), '', $protocol ) . '\']):not([href^=\'#\']):not([href^=\'/\']):not(.no-external-icon)::after { 
			content: \'\e877\';
			font-family: \'mfn-icons\';
			font-style: normal;
			font-weight: 400;
			font-size: 0.8em;
			position: relative;
			margin-left: 0.5em;
			left: unset;
			top: unset;
			background: unset !important;
		}';
	wp_add_inline_style( 'epra-style', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'resonator_external_link_marker', 103 );

/**
 * Adds colour styles when tuned on in page options
 *
 * @return void
 */
function resonator_dynamic_section_styles() {
	global $post;
	if ( ! isset( $post ) ) {
		return;
	}

	$enabled = get_post_meta( $post->ID, 'section_settings_colour_enabled', true );
	if ( empty( $enabled ) ) {
		return;
	}

	$colour = get_post_meta( $post->ID, 'section_settings_colour', true );

	if ( $post->post_parent ) {
		$parents = get_post_ancestors( $post );

		foreach ( $parents as $parent ) {
			$enabled = get_post_meta( $parent, 'section_settings_colour_enabled', true );

			if ( '2' === $enabled ) {
				$colour = get_post_meta( $parent, 'section_settings_colour', true );
				break;
			}
		}
	}

	if ( ! empty( $colour ) ) {
		ob_start();
		?>
			.section-colour {
				background-color: <?php echo esc_attr( $colour ); ?>
			}
		<?php

		$custom_css = ob_get_clean();
		wp_add_inline_style( 'epra-style', $custom_css );
	}
}
add_action( 'wp_enqueue_scripts', 'resonator_dynamic_section_styles', 102 );

// phpcs:disable
/**
 * Header meta tags
 * Orginally found in functions/theme-head.php
 * Change default icon location
 */

function mfn_meta() {
	// viewport
	if ( mfn_opts_get( 'responsive' ) ) {
		if ( mfn_opts_get( 'responsive-zoom' ) ) {
			echo '<meta name="viewport" content="width=device-width, initial-scale=1" />' . "\n";
		} else {
			echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />' . "\n";
		}
	}

	// favicon
	// changed img to images
	if ( mfn_opts_get( 'favicon-img' ) || ( ! has_site_icon() ) ) {
		echo '<link rel="shortcut icon" href="' . esc_url( mfn_opts_get( 'favicon-img', get_theme_file_uri( '/dist/img/favicon.ico' ) ) ) . '" type="image/x-icon" />' . "\n";
	}

	// Apple touch icon
	// Removed the if statement, added fallback, changed img to images
	echo '<link rel="apple-touch-icon" href="' . esc_url( mfn_opts_get( 'apple-touch-icon', get_theme_file_uri( '/dist/img/apple-touch-icon.png' ) ) ) . '" />' . "\n";
}
// phpcs:enable

/**
 * GET ORGINAL THEME-HEAD.PHP
 */
require_once get_template_directory() . '/functions/theme-head.php';
