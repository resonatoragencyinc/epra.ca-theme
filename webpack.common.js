const webpack = require('webpack'); // to access built-in plugins
const path = require('path');

// include the cleaner plugin
const CleanWebpackPlugin = require('clean-webpack-plugin');

// include the js minification plugin
const TerserJSPlugin = require('terser-webpack-plugin');

// include the css extraction and minification plugins
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// include the stylelint plugin
const StyleLintPlugin = require('stylelint-webpack-plugin');

// include the images optimization plugins
const ImageminPlugin = require('imagemin-webpack-plugin').default
const imageminMozjpeg = require('imagemin-mozjpeg');

// include the file copy plugin
const CopyWebpackPlugin = require('copy-webpack-plugin');

// include modernizr for browser support checking
const modernizr = require('modernizr');

// include svgstore to combine svgs
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');

module.exports = {
  entry: {
    'main': ['./js/main.js'],
    'footer': ['./js/footer.js'],
    'style': ['./css/main.scss'],
    'fr_ca': ['./css/fr.scss'],
    'page-meta': ['./js/page-meta.js'],
  },
  context: path.resolve(__dirname, 'src'),
  output: {
    filename: './js/[name].js',
    path: path.resolve(__dirname, 'dist/')
  },
  module: {
    rules: [
      {
        loader: 'webpack-modernizr-loader',
        test: /\.modernizrrc\.js$/
      },
      // perform js babelization on all .js files
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
         }
        }
      },
      // compile all .scss files to plain old css
      {
        test: /\.(scss)$/,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {sourceMap: true},
          },
          {
            loader: 'sass-loader',
            options: {sourceMap: true},
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.ProgressPlugin(),
    // clean dist folder
    new CleanWebpackPlugin({
      //cleanOnceBeforeBuildPatterns: ['!svg/sprites.svg'],
    }),
    // lint the scss
    new StyleLintPlugin({
      configFile: '.stylelint',
      context: 'src',
      files: '**/*.scss',
      failOnError: false,
      quiet: false,
      syntax: 'scss',
    }),
    // extract css into dedicated file
    new MiniCssExtractPlugin({
      filename: './css/[name].min.css',
      chunkFilename: '[id].css'
    }),
    // copy images
    new CopyWebpackPlugin([{
        from: 'img',
        to: 'img'
      }],
      {
        copyUnmodified: true,
        ignore: ['.DS_Store']
      }
    ),
    // combine svgs
    new SVGSpritemapPlugin(
      'src/svg/**/*.svg',
      {
        output: {
          filename: 'svg/sprites.svg',
          svgo: true,
          svg4everybody: {
            polyfill: true
          }
        },
        sprite: {
          generate: {
            use: true
          }
        }
      }
    ),
    //optimize images
    new ImageminPlugin({
      disable: false,
      test: /\.(jpe?g|png|gif)$/i,
      pngquant: {quality: '30-50'},
      plugins: [imageminMozjpeg({quality: 50})]
    })
  ],
  optimization: {
    minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
  },
  resolve: {
    alias: {
      modernizr$: path.resolve(__dirname, '.modernizrrc.js')
    }
  }
};
