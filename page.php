<?php
/**
 * The template for displaying all pages.
 *
 * @package EPRA
 * @author Resonator
 * @link https://muffingroup.com
 */

get_header();
?>

<div id="Content">
	<!-- Remove SVG tempurary -->
	<!-- <div class="content_wrapper clearfix" id="circuitboard">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="200 950 1116.4 2370.9" class="sprite sprite-circuitboard">
		  	<use xlink:href="<?php echo esc_html( get_stylesheet_directory_uri() ); ?>/dist/svg/sprites.svg#sprite-circuitboard"></use>
		</svg>
	</div> -->
	<div class="content_wrapper clearfix">

		<div class="sections_group">

			<div class="entry-content" itemprop="mainContentOfPage">

				<?php
				while ( have_posts() ) {

					$parent_id    = wp_get_post_parent_id( $post->ID );
					$parent_title = '';
					if ( $parent_id ) {
						$parent_title = get_the_title( $parent_id ) . ' - ';
					}

					// wp_get_nav_menu_object.

					if ( ! is_front_page() ) {

						if ( has_post_thumbnail() ) {
							echo '<div class="featured_image">';
							the_post_thumbnail( 'full' );
							echo '</div>';
						}

						echo '<section class="section_wrapper">';
						echo '<h1 class="page_title">';

						the_title( $parent_title . '<span>', '</span>' );

						echo '</h1>';

						echo esc_html( mfn_breadcrumbs() );
						echo '</section>';
					}

					the_post();

					$mfn_builder = new Mfn_Builder_Front( get_the_ID() );
					$mfn_builder->show();

				}

				?>

				<div class="section section-page-footer">
					<div class="section_wrapper clearfix">

						<div class="column one page-pager">
							<?php
								wp_link_pages(
									array(
										'before'         => '<div class="pager-single">',
										'after'          => '</div>',
										'link_before'    => '<span>',
										'link_after'     => '</span>',
										'next_or_number' => 'number',
									)
								);
								?>
						</div>

					</div>
				</div>

			</div>

			<?php if ( mfn_opts_get( 'page-comments' ) ) : ?>
				<div class="section section-page-comments">
					<div class="section_wrapper clearfix">

						<div class="column one comments">
							<?php comments_template( '', true ); ?>
						</div>

					</div>
				</div>
			<?php endif; ?>

		</div>

		<?php get_sidebar(); ?>

	</div>
</div>

<?php
get_footer();
